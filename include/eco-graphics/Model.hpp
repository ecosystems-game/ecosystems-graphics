#ifndef ECOSYSTEMS_MODEL_HPP
#define ECOSYSTEMS_MODEL_HPP

#include <EcoApiModule.h>

#include <eco-graphics/Device.hpp>
#include <eco-graphics/Buffer.hpp>


#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <string>

namespace ecosystems::graphics {
    class Model {
    public:
        struct Vertex {
            glm::vec3 position{};
            glm::vec3 color{1.f};
            glm::vec3 normal{};
            glm::vec2 uv{};

            static std::vector<VkVertexInputBindingDescription> get_binding_descriptions();
            static std::vector<VkVertexInputAttributeDescription> get_attribute_descriptions();

            bool operator==(const Vertex& other) const {
                return position == other.position &&
                    color == other.color &&
                    normal == other.normal &&
                    uv == other.uv;
            }
        };

        struct Builder {
            std::vector<Vertex> vertices{};
            std::vector<uint32_t> indices{};

            void loadModel(const std::string &p_file_path);
        };

        ECO_API Model(Device& p_device, const Builder& builder);
        ECO_API ~Model();

        Model(const Model&)=delete;
        Model &operator=(const Model &)=delete;

        ECO_API void Bind(VkCommandBuffer p_command_buffer);
        ECO_API void Draw(VkCommandBuffer p_command_buffer);

        ECO_API static std::unique_ptr<Model> createModelFromFile(Device& p_device, const std::string& p_file_path);
        ECO_API static std::shared_ptr<Model> createSharedModelFromFile(Device& p_device, const std::string& p_file_path);
    private:
        void CreateVkVertexBuffer(const std::vector<Vertex> &p_vertices);
        void CreateVkIndexBuffer(const std::vector<uint32_t> &p_indices);

        Device& m_device;

        bool m_has_index_buffer = false;
        std::unique_ptr<Buffer> m_vertex_buffer, m_index_buffer;
        uint32_t m_vertex_count, m_index_count;
    };
}

#endif //ECOSYSTEMS_MODEL_HPP
