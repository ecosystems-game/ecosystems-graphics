#ifndef ECOSYSTEMS_SFMLWINDOW_HPP
#define ECOSYSTEMS_SFMLWINDOW_HPP

#include <optional>
#include <memory>
#include <vector>
#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>
#include "EcoApiModule.h"
#include "RenderWindow.hpp"

namespace ecosystems::graphics {

    class RenderWindow {
    public:
        ECO_API RenderWindow(int p_width, int p_height, const char* p_title);
        ECO_API ~RenderWindow();

        RenderWindow(const RenderWindow &) = delete;
        RenderWindow &operator=(const RenderWindow&)=delete;

        ECO_API void CreateVkSurface(VkInstance p_instance, VkSurfaceKHR* p_surface);
        ECO_API virtual bool is_open() { return !glfwWindowShouldClose(m_window); }
        ECO_API bool was_window_resized(){ return m_frame_buffer_resized; }
        ECO_API VkExtent2D get_extent() { return { static_cast<uint32_t>(m_width), static_cast<uint32_t>(m_height) }; }
        ECO_API void reset_window_resized_flag(){ m_frame_buffer_resized = false; }
        ECO_API GLFWwindow* get_glfw_window_ptr(){ return m_window; };
        ECO_API void ResizeFrameBuffer(int p_width, int p_height);
    private:
        int m_width, m_height;
        bool m_frame_buffer_resized = false;
        const int MAX_FRAMES_IN_FLIGHT = 2;
        GLFWwindow* m_window;

    };
}

#endif //ECOSYSTEMS_SFMLWINDOW_HPP
