#include <EcoApiModule.h>
#include <eco-graphics/Shader.hpp>
#include <eco-graphics/Model.hpp>
#include <eco-graphics/Material.hpp>
#include <eco-graphics/Mesh.hpp>
#include <eco-graphics/Renderer.hpp>
#include <eco-graphics/exceptions/GraphicsResourceException.hpp>

#include <memory>
#include <map>
#include <string>


#ifndef ECOSYSTEMS_GRAPHICS_GRAPHICSRESOURCEMANAGER_HPP
#define ECOSYSTEMS_GRAPHICS_GRAPHICSRESOURCEMANAGER_HPP


namespace ecosystems::graphics {
    class GraphicsResourceManager {
    public:
        ECO_API explicit GraphicsResourceManager(Device& p_device) : m_device(p_device) {}
        ECO_API ~GraphicsResourceManager() {
            // Ensure destruction happens from the top-down
            ClearAllResources();
        }

        ECO_API void LoadResourcesFromFile(const char* p_resource_file, Renderer& p_renderer);
        ECO_API void ClearAllResources() {
            m_meshes.clear();
            m_models.clear();
            m_materials.clear();
            m_pipelines.clear();
            m_shaders.clear();
        }

        ECO_API void LoadShader(std::string p_shader_tag, std::string p_shader_file, ShaderType p_shader_type);
        ECO_API void LoadPipeline(std::string p_pipeline_tag, ShaderCollection& p_shader_collection, Renderer& p_renderer);
        ECO_API void LoadMaterial(std::string p_mat_tag, Pipeline& p_pipeline);
        ECO_API void LoadModel(std::string p_model_tag, const char* p_model_file);
        ECO_API void LoadMesh(std::string p_mesh_tag, Model& p_model, Material& p_material);

        ECO_API Shader& get_shader(std::string p_shader_tag);
        ECO_API std::shared_ptr<Shader>& get_shader_ptr(std::string p_shader_tag);
        ECO_API Pipeline& get_pipeline(std::string p_pipeline_tag);
        ECO_API Material& get_material(std::string p_material_tag);
        ECO_API Model& get_model(std::string p_model_tag);
        ECO_API Mesh& get_mesh(std::string p_mesh_tag);
    private:
        ShaderType GetShaderType(std::string& p_type) {
            if(p_type == "vertex") return ShaderType::Vertex;
            if(p_type == "fragment") return ShaderType::Fragment;

            throw exceptions::GraphicsResourceException("Shader Type not found!");
        }

        Device& m_device;
        std::map<std::string, std::shared_ptr<Shader>> m_shaders;
        std::map<std::string, std::unique_ptr<Pipeline>> m_pipelines;
        std::map<std::string, std::unique_ptr<Material>> m_materials;
        std::map<std::string, std::unique_ptr<Model>> m_models;
        std::map<std::string, std::unique_ptr<Mesh>> m_meshes;
    };
}

#endif //ECOSYSTEMS_GRAPHICS_GRAPHICSRESOURCEMANAGER_HPP
