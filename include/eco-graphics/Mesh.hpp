#ifndef ECOSYSTEMS_GRAPHICS_MESH_HPP
#define ECOSYSTEMS_GRAPHICS_MESH_HPP

#include <eco-graphics/Model.hpp>
#include <eco-graphics/Material.hpp>
#include <eco-graphics/FrameInfo.hpp>


namespace ecosystems::graphics {
    class Mesh {
    public:
        ECO_API explicit Mesh(Model& p_model, Material& p_material);
        ECO_API virtual void Render (
                const glm::mat4 &p_model_matrix,
                FrameInfo &p_frame_info);
    private:
        Model& m_model;
        Material& m_material;
    };
}
#endif // ECOSYSTEMS_GRAPHICS_MESH_HPP
