#include <exception>

#ifndef ECOSYSTEMS_GRAPHICS_GRAPHICSRESOURCEEXCEPTION_HPP
#define ECOSYSTEMS_GRAPHICS_GRAPHICSRESOURCEEXCEPTION_HPP

namespace ecosystems::graphics::exceptions {
    class GraphicsResourceException : public std::runtime_error {
    public:
        explicit GraphicsResourceException(const char *p_message) : runtime_error(p_message) {}
    };
}

#endif //ECOSYSTEMS_GRAPHICS_GRAPHICSRESOURCEEXCEPTION_HPP
