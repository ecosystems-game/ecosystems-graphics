#ifndef ECOSYSTEMS_SWAPCHAIN_HPP
#define ECOSYSTEMS_SWAPCHAIN_HPP

#include "Device.hpp"


namespace ecosystems::graphics {
    class SwapChain {
    public:
        static constexpr int MAX_FRAMES_IN_FLIGHT = 2;

        SwapChain(Device& p_device, VkExtent2D p_extent);
        SwapChain(Device &p_device, VkExtent2D p_extent, std::shared_ptr<SwapChain> p_prev);
        ~SwapChain();

        SwapChain(const SwapChain&)=delete;
        void operator=(const SwapChain&)=delete;

        VkFramebuffer get_frame_buffer(int index) { return m_swap_chain_frame_buffers[index]; }
        VkRenderPass get_render_pass() { return m_render_pass; }
        VkImageView get_image_view(int index) { return m_swap_chain_image_views[index]; }
        size_t get_image_count() { return m_swap_chain_images.size(); }
        VkSwapchainKHR get_swap_chain() { return m_swap_chain; }
        VkFormat get_swap_chain_image_format() { return m_swap_chain_image_format; }
        VkExtent2D get_swap_chain_extent() { return m_swap_chain_extent; }

        uint32_t width() { return m_swap_chain_extent.width; }
        uint32_t height() { return m_swap_chain_extent.height; }

        [[nodiscard]] float extent_aspect_ratio() const {
            return static_cast<float>(m_swap_chain_extent.width) / static_cast<float>(m_swap_chain_extent.height);
        }

        VkFormat FindDepthFormat();
        VkResult AcquireNextImage(uint32_t* p_image_index);
        VkResult SubmitVkCommandBuffers(const VkCommandBuffer* p_buffers, uint32_t* p_image_index);

        bool CompareSwapFormats(const SwapChain &p_swap_chain) const {
            return p_swap_chain.m_swap_chain_depth_format == m_swap_chain_depth_format &&
                   p_swap_chain.m_swap_chain_image_format == m_swap_chain_image_format;
        }
    private:
        void Init();
        void CreateVkSwapChain();
        void CreateVkImageViews();
        void CreateVkDepthResources();
        void CreateVkRenderPass();
        void CreateVkFrameBuffers();
        void CreateVkSyncObjects();

        // Helper Functions
        VkSurfaceFormatKHR ChooseVkSwapSurfaceFormats(const std::vector<VkSurfaceFormatKHR>& availableFormats);
        VkPresentModeKHR ChooseVkSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes);
        VkExtent2D ChooseVkSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities);

        VkFormat m_swap_chain_image_format, m_swap_chain_depth_format;
        VkExtent2D m_swap_chain_extent;
        std::vector<VkFramebuffer> m_swap_chain_frame_buffers;
        VkRenderPass m_render_pass;
        std::vector<VkImage> m_depth_images;
        std::vector<VkDeviceMemory> m_depth_image_memories;
        std::vector<VkImageView> m_depth_image_views;
        std::vector<VkImage> m_swap_chain_images;
        std::vector<VkImageView> m_swap_chain_image_views;

        Device& m_device;
        VkExtent2D m_window_extent;

        VkSwapchainKHR m_swap_chain;
        std::shared_ptr<SwapChain> m_old_swap_chain;

        std::vector<VkFence> m_in_flight_fences, m_images_in_flight;
        std::vector<VkSemaphore> m_image_available_semaphores, m_render_finished_semaphores;
        size_t m_current_frame = 0;
    };
}

#endif //ECOSYSTEMS_SWAPCHAIN_HPP
