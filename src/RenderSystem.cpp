#include "eco-graphics/RenderSystem.hpp"
#include "eco-graphics/RenderWindow.hpp"
#include "eco-graphics/Model.hpp"


namespace ecosystems::graphics {

    struct SimplePushConstantData {
        glm::mat4 transform{1.f};
        glm::mat4 modelMatrix{1.f};
    };


    RenderSystem::RenderSystem(Device& p_device,
                               ShaderCollection& p_shaders,
                               VkRenderPass p_render_pass)
        : m_device{p_device}, m_shader_collection(p_shaders) {}

    RenderSystem::~RenderSystem() {}

    void RenderSystem::CreatePipeline(VkRenderPass p_render_pass) {
        std::cerr << "[SHADER] [ERROR] [NON_SEVERE] Do not create VkPipelines using the parent RenderSystem class. Please create a new abstract class which inherits RenderSystem instead! (See SimpleRenderSystem in the ecosystems-graphics library for an example.)" << std::endl;
    }

    void RenderSystem::BindCommandBuffer(VkCommandBuffer p_command_buffer) {
        m_pipeline->Bind(p_command_buffer);
    }

    void RenderSystem::Render(VkCommandBuffer p_command_buffer,
                              const glm::mat4& p_model_matrix,
                              Model& p_model,
                              const glm::mat4& p_projection_view) {
        std::cerr << "[SHADER] [ERROR] [NON_SEVERE] Do not Render from the RenderSystem parent class. Please create an abstract class inheriting shader instead! (See SimpleRenderSystem in the ecosystems-graphics library for an example.)" << std::endl;
    }
}