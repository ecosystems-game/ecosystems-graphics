#include "eco-graphics/util.hpp"
#include <set>
#include <cstring>
#include <cstdio>
#include <optional>
#include <iostream>
#include <stdexcept>
#include <cstdlib>
#include <vector>
#include <cstdint>
#include <algorithm>
#include <vulkan/vulkan.h>
#define VK_USE_PLATFORM_WIN32_KHR
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#define GLFW_EXPOSE_NATIVE_WIN32
#include "eco-graphics/RenderWindow.hpp"

namespace ecosystems::graphics {
    RenderWindow::RenderWindow(int p_width, int p_height, const char* p_title)
        : m_width(p_width), m_height(p_height)
    {
        glfwInit();
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
        m_window = glfwCreateWindow(p_width, p_height, p_title, nullptr, nullptr);

        glfwSetWindowUserPointer(m_window, this);
    }

    RenderWindow::~RenderWindow() {
        glfwDestroyWindow(m_window);
        glfwTerminate();
    }

    void RenderWindow::CreateVkSurface(VkInstance p_instance, VkSurfaceKHR* p_surface) {
        if(glfwCreateWindowSurface(p_instance, m_window, nullptr, p_surface) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create window surface!");
        }
    }

    void RenderWindow::ResizeFrameBuffer(int p_width, int p_height) {
        m_frame_buffer_resized = true;
        m_width = p_width;
        m_height = p_height;
    }
}