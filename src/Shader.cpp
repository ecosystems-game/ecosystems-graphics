#include <eco-graphics/Shader.hpp>


namespace ecosystems::graphics {
    Shader::Shader(std::string& p_shader_file, const char* p_shader_name, ShaderType p_shader_type, Device &p_device)
            : m_device(p_device), m_shader_file(p_shader_file), m_shader_type(p_shader_type), m_shader_name(p_shader_name)
    {
        CreateVkShaderModule(readFile(p_shader_file), &m_vk_shader_module);
    }

    Shader::~Shader() {
        vkDestroyShaderModule(m_device.get_device(), m_vk_shader_module, nullptr);
    };

    void Shader::CreateVkShaderModule(const std::vector<char> &code, VkShaderModule *p_shader_module) {
        VkShaderModuleCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        createInfo.codeSize = code.size();
        createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());

        if (vkCreateShaderModule(m_device.get_device(), &createInfo, nullptr, p_shader_module) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create shader module!");
        }
    }
}