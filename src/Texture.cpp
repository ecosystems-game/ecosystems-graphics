
#include "eco-graphics/Texture.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include <thirdparty/stb_image.h>

#include <vulkan/vulkan.hpp>

namespace ecosystems::graphics {

    std::unique_ptr<Texture> Texture::CreateTextureFromImageFile(Device& p_device, const char *p_image_file) {
        int texWidth, texHeight, texChannels;
        stbi_uc* pixels = stbi_load(p_image_file, &texWidth, &texHeight, &texChannels, STBI_ORDER_RGB);

        VkDeviceSize imageSize = texWidth*texHeight*4;
        if(!pixels) {
            throw std::runtime_error("Failed to load texture from image!");
        }
        VkBuffer stagingBuffer;
        VkDeviceMemory stagingBufferMemory;
        p_device.CreateVkBuffer(imageSize,
                                VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                                stagingBuffer,
                                stagingBufferMemory);
        void* data;
        vkMapMemory(p_device.get_device(), stagingBufferMemory, 0, imageSize, 0, &data);
        memcpy(data, pixels, static_cast<size_t>(imageSize));
        vkUnmapMemory(p_device.get_device(), stagingBufferMemory);

        stbi_image_free(pixels);

        auto texture = std::make_unique<Texture>(p_device);
        CreateImage(p_device,
                      texWidth,
                      texHeight,
                      VK_FORMAT_R8G8B8A8_SRGB,
                      VK_IMAGE_TILING_OPTIMAL,
                      VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                      VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                      texture->get_texture_image(),
                      texture->get_texture_image_memory());
        return texture;
    }

    void Texture::CreateImage(Device& p_device, uint32_t p_width, uint32_t p_height, VkFormat p_format, VkImageTiling p_tiling,
                                VkImageUsageFlags p_usage, VkMemoryPropertyFlags p_properties, VkImage &p_image,
                                VkDeviceMemory &p_image_memory) {
        VkImageCreateInfo imageInfo{};
        imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        imageInfo.imageType = VK_IMAGE_TYPE_2D;
        imageInfo.extent.width = p_width;
        imageInfo.extent.height = p_height;
        imageInfo.extent.depth = 1;
        imageInfo.mipLevels = 1;
        imageInfo.arrayLayers = 1;
        imageInfo.format = p_format;
        imageInfo.tiling = p_tiling;
        imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        imageInfo.usage = p_usage;
        imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
        imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

        if (vkCreateImage(p_device.get_device(), &imageInfo, nullptr, &p_image) != VK_SUCCESS) {
            throw std::runtime_error("failed to create image!");
        }

        VkMemoryRequirements memRequirements;
        vkGetImageMemoryRequirements(p_device.get_device(), p_image, &memRequirements);

        VkMemoryAllocateInfo allocInfo{};
        allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        allocInfo.allocationSize = memRequirements.size;
        allocInfo.memoryTypeIndex = p_device.FindMemoryType(memRequirements.memoryTypeBits, p_properties);

        if (vkAllocateMemory(p_device.get_device(), &allocInfo, nullptr, &p_image_memory) != VK_SUCCESS) {
            throw std::runtime_error("failed to allocate image memory!");
        }

        vkBindImageMemory(p_device.get_device(), p_image, p_image_memory, 0);
    }
}