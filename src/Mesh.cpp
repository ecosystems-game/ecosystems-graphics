#include <eco-graphics/Mesh.hpp>
#include <eco-graphics/PushConstant.hpp>

namespace ecosystems::graphics {

    Mesh::Mesh(Model& p_model, Material& p_material)
        : m_model(p_model), m_material(p_material) {}

    void Mesh::Render(const glm::mat4 &p_model_matrix,
                      FrameInfo &p_frame_info) {
        m_model.Bind(p_frame_info.command_buffer);

        PushConstant push{};
        push.transform = p_frame_info.projection_view * p_model_matrix;
        push.modelMatrix = p_model_matrix;

        vkCmdPushConstants(
                p_frame_info.command_buffer,
                m_material.get_pipeline().get_pipeline_layout(),
                m_material.get_pipeline().get_stage_flags(),
                0,
                sizeof(PushConstant),
                &push);
        m_material.Bind(p_frame_info.command_buffer);
        m_model.Draw(p_frame_info.command_buffer);
    }
}