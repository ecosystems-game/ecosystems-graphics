#include "eco-graphics/Model.hpp"

#define TINYOBJLOADER_IMPLEMENTATION
#include "thirdparty/TinyObjectLoader.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp>
#include "eco-graphics/util.hpp"

#include <cassert>
#include <cstring>
#include <memory>
#include <unordered_map>


namespace std {
    template <> struct hash<ecosystems::graphics::Model::Vertex> {
        size_t operator()(ecosystems::graphics::Model::Vertex const &vertex) const {
            size_t seed = 0;
            hashCombine(seed, vertex.position, vertex.color, vertex.normal, vertex.uv);
            return seed;
        }
    };
}

namespace ecosystems::graphics {

    Model::Model(Device &p_device, const Builder &p_builder) : m_device(p_device) {
        CreateVkVertexBuffer(p_builder.vertices);
        CreateVkIndexBuffer(p_builder.indices);
    }

    Model::~Model() {}

    void Model::Bind(VkCommandBuffer p_command_buffer) {
        VkBuffer buffers[] = {m_vertex_buffer->get_buffer()};
        VkDeviceSize offsets[] = {0};
        vkCmdBindVertexBuffers(p_command_buffer, 0, 1, buffers, offsets);

        if(m_has_index_buffer) {
            vkCmdBindIndexBuffer(p_command_buffer, m_index_buffer->get_buffer(), 0, VK_INDEX_TYPE_UINT32);
        }
    }

    void Model::Draw(VkCommandBuffer p_command_buffer) {
        if(m_has_index_buffer) {
            vkCmdDrawIndexed(p_command_buffer, m_index_count, 1, 0, 0, 0);
        } else {
            vkCmdDraw(p_command_buffer, m_vertex_count, 1, 0, 0);
        }
    }

    void Model::CreateVkVertexBuffer(const std::vector<Vertex> &p_vertices) {
        m_vertex_count = static_cast<uint32_t>(p_vertices.size());
        assert(m_vertex_count >= 3 && "Vertex count must be at least 3!");

        VkDeviceSize bufferSize = sizeof(p_vertices[0]) * m_vertex_count;
        uint32_t vertexSize = sizeof(p_vertices[0]);

        Buffer stagingBuffer(
                m_device,
                vertexSize,
                m_vertex_count,
                VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

        stagingBuffer.Map();
        stagingBuffer.WriteToBuffer((void *) p_vertices.data());

        m_vertex_buffer = std::make_unique<Buffer>(
                m_device,
                vertexSize,
                m_vertex_count,
                VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

        m_device.CopyVkBuffer(stagingBuffer.get_buffer(), m_vertex_buffer->get_buffer(), bufferSize);
    }

    void Model::CreateVkIndexBuffer(const std::vector<uint32_t> &p_indices) {
        m_index_count = static_cast<uint32_t>(p_indices.size());
        m_has_index_buffer = m_index_count > 0;

        if(!m_has_index_buffer) return;
        assert(m_index_count >= 3 && "Index count must be at least 3!");

        VkDeviceSize bufferSize = sizeof(p_indices[0]) * m_index_count;
        uint32_t indexSize = sizeof(p_indices[0]);

        Buffer stagingBuffer(
                m_device,
                indexSize,
                m_index_count,
                VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
        stagingBuffer.Map();
        stagingBuffer.WriteToBuffer((void *) p_indices.data());

        m_index_buffer = std::make_unique<Buffer>(
                m_device,
                indexSize,
                m_index_count,
                VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

        m_device.CopyVkBuffer(stagingBuffer.get_buffer(), m_index_buffer->get_buffer(), bufferSize);
    }

    std::unique_ptr<Model> Model::createModelFromFile(Device& p_device, const std::string &p_file_path) {
        Builder builder{};
        builder.loadModel(p_file_path);
        return std::make_unique<Model>(p_device, builder);
    }

    std::shared_ptr<Model> Model::createSharedModelFromFile(Device &p_device, const std::string &p_file_path) {
        Builder builder{};
        builder.loadModel(p_file_path);
        return std::make_shared<Model>(p_device, builder);
    }

    std::vector<VkVertexInputBindingDescription> Model::Vertex::get_binding_descriptions() {
        std::vector<VkVertexInputBindingDescription> bindingDescriptions(1);
        bindingDescriptions[0].binding = 0;
        bindingDescriptions[0].stride = sizeof(Vertex);
        bindingDescriptions[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
        return bindingDescriptions;
    }

    std::vector<VkVertexInputAttributeDescription> Model::Vertex::get_attribute_descriptions() {
        std::vector<VkVertexInputAttributeDescription> attributeDescriptions({});

        // {{Location, Binding, Format, Offset}}
        attributeDescriptions.push_back({0, 0, VK_FORMAT_R32G32B32_SFLOAT, offsetof(Vertex, position)});
        attributeDescriptions.push_back({1, 0, VK_FORMAT_R32G32B32_SFLOAT, offsetof(Vertex, color)});
        attributeDescriptions.push_back({2, 0, VK_FORMAT_R32G32B32_SFLOAT, offsetof(Vertex, normal)});
        attributeDescriptions.push_back({3, 0, VK_FORMAT_R32G32B32_SFLOAT, offsetof(Vertex, uv)});

        return attributeDescriptions;
    }

    void Model::Builder::loadModel(const std::string &p_file_path) {
        tinyobj::attrib_t attrib;
        std::vector<tinyobj::shape_t> shapes;
        std::vector<tinyobj::material_t> materials;
        std::string warn, err;

        if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, p_file_path.c_str())) {
            throw std::runtime_error("Failed to load model! " + warn + err);
        }

        vertices.clear();
        indices.clear();

        std::unordered_map<Vertex, uint32_t> uniqueVertices{};
        for(const auto& shape : shapes) {
            for(const auto &index : shape.mesh.indices) {
                Vertex vertex{};

                if(index.vertex_index >= 0) {
                    vertex.position = {
                            attrib.vertices[3*index.vertex_index + 0],
                            attrib.vertices[3*index.vertex_index + 1],
                            attrib.vertices[3*index.vertex_index + 2],
                    };

                    vertex.color = {
                            attrib.colors[3*index.vertex_index + 0],
                            attrib.colors[3*index.vertex_index + 1],
                            attrib.colors[3*index.vertex_index + 2],
                    };
                }

                if(index.normal_index >= 0) {
                    vertex.normal = {
                            attrib.normals[3*index.normal_index + 0],
                            attrib.normals[3*index.normal_index + 1],
                            attrib.normals[3*index.normal_index + 2],
                    };
                }

                if(index.texcoord_index >= 0) {
                    vertex.uv = {
                            attrib.texcoords[2*index.texcoord_index + 0],
                            attrib.texcoords[2*index.texcoord_index + 1],
                    };
                }

                if(uniqueVertices.count(vertex) == 0) {
                    uniqueVertices[vertex] = static_cast<uint32_t>(vertices.size());
                    vertices.push_back(vertex);
                }
                indices.push_back(uniqueVertices[vertex]);
            }
        }
    }
}