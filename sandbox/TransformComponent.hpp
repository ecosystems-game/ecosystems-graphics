#ifndef ECOSYSTEMS_TRANSFORMCOMPONENT_HPP
#define ECOSYSTEMS_TRANSFORMCOMPONENT_HPP

#include <glm/gtc/matrix_transform.hpp>


namespace ecosystems::graphics::test {
    struct TransformComponent {
        glm::vec3 translation{};
        float scale{ 1.f };
        glm::vec3 rotation{};

        // Matrix corresponds to Translate * Ry * Rx * Rz * Scale
        // Rotations correspond to Tait-bryan angles of Y(1), X(2), Z(3)
        // https://en.wikipedia.org/wiki/Euler_angles#Rotation_matrix
        glm::mat4 mat4() {
            const float z_cos = glm::cos(rotation.z);
            const float z_sin = glm::sin(rotation.z);
            const float x_cos = glm::cos(rotation.x);
            const float x_sin = glm::sin(rotation.x);
            const float y_cos = glm::cos(rotation.y);
            const float y_sin = glm:: sin(rotation.y);
            return glm::mat4{
                    {scale * (y_cos * z_cos + y_sin * x_sin * z_sin), scale * (x_cos * z_sin), scale * (y_cos * x_sin * z_sin - z_cos * y_sin), 0.0f,},
                    {scale * (z_cos * y_sin * x_sin - y_cos * z_sin), scale * (x_cos * z_cos), scale * (y_cos * z_cos * x_sin + y_sin * z_sin), 0.0f,},
                    {scale * (x_cos * y_sin),scale * (-x_sin),scale * (y_cos * x_cos),0.0f,},
                    {translation.x, translation.y,translation.z,1.0f} };
        }

        TransformComponent() = default;
        TransformComponent(const TransformComponent&) = default;
        TransformComponent(const glm::vec3& p_translation, float p_scale, const glm::vec3& p_rotation)
            : translation(p_translation), scale(p_scale), rotation(p_rotation) {}
    };
}

#endif //ECOSYSTEMS_TRANSFORMCOMPONENT_HPP
