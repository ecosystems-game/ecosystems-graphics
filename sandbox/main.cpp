#include <map>
#include <iostream>

#include <eco-graphics/Mesh.hpp>
#include <eco-graphics/Renderer.hpp>
#include <eco-graphics/RenderWindow.hpp>
#include <eco-graphics/Device.hpp>
#include <eco-graphics/FrameInfo.hpp>
#include <eco-graphics/GuiLayer.hpp>
#include <eco-graphics/GraphicsResourceManager.hpp>
#include <eco-graphics/gui/UiTheme.hpp>

#include "CameraComponent.hpp"
#include "TransformComponent.hpp"


using namespace ecosystems;

/* * * * * * * * * * * * * * *
 *   Setup Uniform Buffer    *
 * * * * * * * * * * * * * * */
struct GlobalUbo {
    glm::mat4 projection_view{1.f};
    glm::vec3 light_direction = glm::normalize(glm::vec3{1.f, -3.f, -1.f});
};

/* * * * * * * * * * * *
 *     Setup Vulkan    *
 * * * * * * * * * * * */
static constexpr int WIDTH = 800;
static constexpr int HEIGHT = 600;

graphics::RenderWindow render_window{ WIDTH, HEIGHT, "EcoSystems" };
graphics::Device device{ render_window };
graphics::Renderer renderer{ render_window, device };

/* * * * * * * * * * * *
 *  Application Start  *
 * * * * * * * * * * * */
int main() {
    #if !defined(NDEBUG)
        std::cout << "Running in DEBUG" << std::endl;
    #endif
    // Define and obtain resource objects
    graphics::GraphicsResourceManager graphicsResourceManager(device);
    graphicsResourceManager.LoadResourcesFromFile("data/resources/sandbox.rsrc.json", renderer);
    graphics::Mesh& cubeMesh = graphicsResourceManager.get_mesh("color_cube_mesh");
    graphics::Mesh& vaseMesh = graphicsResourceManager.get_mesh("flat_vase_mesh");

    /* * * * * * * * * * * *
     *  Define UBO Buffer  *
     * * * * * * * * * * * */
    std::vector<std::unique_ptr<graphics::Buffer>> uboBuffers(graphics::SwapChain::MAX_FRAMES_IN_FLIGHT);
    for(int i = 0; i < uboBuffers.size(); ++i) {
        uboBuffers[i] = std::make_unique<graphics::Buffer>(
            device,
            sizeof(GlobalUbo),
            1,
            VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
            device.get_properties().limits.minUniformBufferOffsetAlignment);
        uboBuffers[i]->Map();
    }


    /* * * * * * * * * * * *
     *    RENDER OBJECTS   *
     * * * * * * * * * * * */
    int transformCount = 9;
    std::vector<graphics::test::TransformComponent> transforms(1000);

    float x = -2.0;
    for(auto& transform : transforms) {
        float y = 0.f;
        x += 0.5f;
        float z = -10.f;

        transform.translation = {x, y, z};

        float LO = 0.01;
        float HI = 0.25;

        transform.scale = LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI-LO)));
    }

    // Setup Camera and Camera Transform
    graphics::test::CameraComponent cc;
    graphics::test::TransformComponent ct;

    // These speed variables are used for rotation in the update
    std::vector<float> speeds;
    for(size_t i = 0; i < transforms.size(); ++i) {
        float LO = 0.25;
        float HI = 2.00;
        float r = LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI-LO)));
        speeds.push_back(r);
    }

    /* * * * * * * * * * * *
     *       GAMELOOP      *
     * * * * * * * * * * * */
    auto currentTime = std::chrono::high_resolution_clock::now();
    float direction = 1;
    float counter = 0;
    while (render_window.is_open()) {
        glfwPollEvents();

        /* * * * * * * * * * * * * *
         *   Calculate Delta Time  *
         * * * * * * * * * * * * * */
        auto newTime = std::chrono::high_resolution_clock::now();
        float deltaTime = std::chrono::duration<float, std::chrono::seconds::period>(newTime - currentTime).count();
        currentTime = newTime;
        float timer = 0;
        /* * * * * * * * * * * * * *
         *   Update Cube Rotation  *
         * * * * * * * * * * * * * */
        for(size_t i = 0; i < transforms.size(); ++i) {
            float speed = speeds[i];
            transforms[i].rotation += glm::vec3(speed * deltaTime, speed * deltaTime, speed * deltaTime);
            transforms[i].translation = glm::vec3(
                    transforms[i].translation.x,
                    sin(transforms[i].translation.x+counter),
                    transforms[i].translation.z);

            counter += 0.001f*deltaTime;
        }

        /* * * * * * * * * * * * * *
         *     Render to Screen    *
         * * * * * * * * * * * * * */
        auto projectionView = cc.projection_matrix * cc.view_matrix;
        if (auto commandBuffer = renderer.BeginFrame()) {
            int frameIndex = renderer.get_frame_index();
            graphics::FrameInfo frameInfo {
                frameIndex,
                deltaTime,
                commandBuffer,
                projectionView
            };

            // Update Global UBO with frame information
            GlobalUbo ubo{};
            ubo.projection_view = projectionView;
            uboBuffers[frameIndex]->WriteToIndex(&ubo, frameIndex);
            uboBuffers[frameIndex]->Flush();

            // Render
            renderer.BeginSwapChainRenderPass(commandBuffer);
            int i = 0;
            for(auto& transform : transforms) {
                if(i%2 == 0) {
                    cubeMesh.Render(transform.mat4(), frameInfo);
                } else {
                    cubeMesh.Render(transform.mat4(), frameInfo);
                }
                ++i;
            }
            renderer.EndSwapChainRenderPass(commandBuffer);
            renderer.EndFrame();
        }

        /* * * * * * * * * * * * * *
         *      Update Camera      *
         * * * * * * * * * * * * * */
        cc.setViewTarget(ct.translation, transforms[10].translation);
        cc.setPerspectiveProjection(glm::radians(50.f), renderer.get_aspect_ratio(), 0.1, 30.f);
    }
	return 0;
}